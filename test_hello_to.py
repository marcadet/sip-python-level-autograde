
from test_utilities import *

import random

import hello

def check_hello_to_function():
    print_info( "checking existence of function hello_to() with one argument" )
    if not check_callable( hello, "hello_to", 1 ):
        return False
    
    print_info( "checking the output of function hello_to()" )
    argument = random.choice( ['SIP', 'sip', 'CentraleSupélec', 'SIP@CentraleSupélec', 'sip@CentraleSupélec'] )
    output, result = get_output_and_result_with_argument( hello.hello_to, argument )
    expected = "Hello to " + argument + "!\n"
    if output != expected:
        print_error( "your function hello_to() with the argument " + argument + " must print: " + expected )
        print_error( "it prints: ", output )
        return False
    if result != None:
        print_error( "your function hello_to() must not return something'" )
        print_error( "it returns: ", result )
        return False
    
    print_success( "your function hello_to() does the right thing" )

    return True


if __name__ == "__main__":
    if not check_hello_to_function():
        exit(-1)
    exit(0)

