
from test_utilities import *

import hello

def check_hello_world_function():
    print_info( "checking existence of function hello_world() with no argument" )
    if not check_callable( hello, "hello_world", 0 ):
        return False
    
    print_info( "checking the output of function hello_world()" )
    output, result = get_output_and_result_no_argument( hello.hello_world )
    expected = "Hello world!\n"
    if output != expected:
        print_error( "your function hello_world() must print: " + expected )
        print_error( "it prints: ", output )
        return False
    if result != None:
        print_error( "your function hello_world() must not return something'" )
        print_error( "it returns: ", result )
        return False
    
    print_success( "your function hello_world() does the right thing" )

    return True


if __name__ == "__main__":
    if not check_hello_world_function():
        exit(-1)
    exit(0)

