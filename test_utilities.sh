#!/usr/bin/env bash

TXT_WHITE="\033[37m"
TXT_BRIGHT_RED="\033[91m"
TXT_BRIGHT_GREEN="\033[92m"
TXT_BRIGHT_CYAN="\033[96m"
TXT_CLEAR="\033[0m"

print_info() {
    echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: ${1}${TXT_CLEAR}";
}

print_success() {
    echo -e "  >> ${TXT_BRIGHT_GREEN}SUCCESS: ${1}${TXT_CLEAR}";
}

print_error() {
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: ${1}${TXT_CLEAR}";
}

get_command() {
    return "${TXT_WHITE}${1}${TXT_CLEAR}";
}

