#!/usr/bin/env bash


. test_utilities.sh

########################################################

print_info "checking existence of file hello.py"
if [[ ! -e hello.py ]]; then
    print_error "you must have a python script called hello.py"
    exit -1
fi
print_success "file hello.py is present"

########################################################

print_info "checking syntax of file hello.py"
python3 -m py_compile hello.py
if [[ $? -ne 0 ]]; then
    print_error "your file hello.py is not a valid python script"
    exit -1
fi
print_success "your file hello.py is a valid python script"

########################################################

print_info "checking content of file hello.py"
python3 test_hello_world.py
if [[ $? -ne 0 ]]; then
    exit -1
fi
python3 test_hello_to.py
if [[ $? -ne 0 ]]; then
    exit -1
fi


########################################################

exit 0

