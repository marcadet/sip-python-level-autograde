
import inspect
import io
import sys

TXT_BRIGHT_RED="\x1b[91m"
TXT_BRIGHT_GREEN="\x1b[92m"
TXT_BRIGHT_CYAN="\x1b[96m"
TXT_CLEAR="\x1b[0m"

def print_info( *text ):
    print("  >> " + TXT_BRIGHT_CYAN + "INFO:", *text, TXT_CLEAR)

def print_success( *text ):
    print("  >> " + TXT_BRIGHT_GREEN + "SUCCESS:", *text, TXT_CLEAR)

def print_error( *text ):
    print("  >> " + TXT_BRIGHT_RED + "ERROR: ", *text, TXT_CLEAR)

def check_callable( module, function, args_count ):
    if not function in dir( module ):
        print_error( "you must define a function named " + function + "()" )
        return False
    if not callable( module.__dict__[function] ):
        print_error( "you must define a function named " + function + "()" )
    count = len( inspect.signature( module.__dict__[function] ).parameters )
    if args_count == 0:
        if count != 0:
            print_error( "your function named " + function + " must take no argument" )
            return False
        print_success( "you have one function " + function + " with no argument" )
        return True
    if args_count != count:
        print_error( "your function named " + function + " must take", args_count, " argument(s), it takes", count )
        return False
    print_success( "you have one function " + function + " with", args_count, " argument(s)" )
    return True


def get_output_and_result_no_argument( function ):
    sys.stdout = io.StringIO()
    result = function()
    output = sys.stdout.getvalue()
    sys.stdout.close()
    sys.stdout = sys.__stdout__
    return output, result

def get_output_and_result_with_argument( function, *arguments ):
    sys.stdout = io.StringIO()
    result = function( *arguments )
    output = sys.stdout.getvalue()
    sys.stdout.close()
    sys.stdout = sys.__stdout__
    return output, result
